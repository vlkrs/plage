PLAGE(1) - General Commands Manual

# NAME

**plage** - manage passwords with age

# SYNOPSIS

**plage**
[**-h**]
*command*
[*argument*]

# DESCRIPTION

**plage**
is a password manager.
Every password lives inside a
age(1)
encrypted file somewhere inside
*~/.plage/store*
which is managed with the
got(1)
version control system to keep track of changes, recover accidental
overwrites and synchronize passwords across devices.

The options are as follows:

**-h**

> Display usage information and exit immediately.

The following commands are available:

**cat** *entries ...*

> Decrypt and print the content of
> *entries*
> in the given order.

**edit** *entry*

> Interactively modify the content of the given
> *entry*
> with an editor.

**find** \[*pattern*]

> Print the entries of the store one per line, optionally filtered by
> the case-insensitive
> *pattern*.

**mv** *from* *to*

> Rename a password entry, doesn't work with directories.
> *from*
> must exist and
> *to*
> mustn't.

**rm** *entries ...*

> Remove the given
> *entries*
> from the store.

**tee** \[**-q**] *entry*

> Persist the data read from standard input into the store under the given
> *entry*
> name and then print it again on the standard output unless the
> **-q**
> option is given.

**version**

> Print version and exit.

Password entries can be referenced using the path relative to the
store directory.
The file extension
".age"
can be omitted.

# ENVIRONMENT

`PLAGE_AGE`

> Path to the
> age(1)
> executable.

`PLAGE_STORE`

> Alternative path to the password store directory tree.

`PLAGE_IDENTITY`

> Alternative path to the age identity file.

`PLAGE_RECIPIENTS`

> Alternative path to the age recipients file.

`VISUAL`, `EDITOR`

> The editor spawned by
> **plage**
> **edit**.
> If not set, the
> ed(1)
> text editor will be used to give it the attention it deserves.

# FILES

*~/.plage*

> Default password store base directory.

*~/.plage/.age-id*

> age identity file, this should be either an encrypted age file, or a file
> with key handles generated from age-plugin-yubikey.

*~/.plage/store/*

> Default password store.

*~/.plage/store/.recipients*

> age recipients file containing the recipient(s) to which the password are
> encrypted.

# EXIT STATUS

The **plage** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# EXAMPLES

A got repository and password store can be initialized as follows:

	$ mkdir -p ~/.plage/store
	$ KEY="$(age-keygen)"
	$ echo "$KEY" | age -p -a >> ~/.plage/.age-id
	$ echo "$KEY" | age-keygen -y >> ~/.plage/store/.recipients
	$ gotadmin init ~/got/plage.git
	$ got import -r ~/got/plage.git -m 'initial import' ~/.plage/store
	$ got checkout -E ~/got/plage.git ~/.plage/store

see
got(1)
for more information.

Generate a random password and save it to the clipboard:

	$ pwg | plage tee entry/name | xsel -b

Generate a TOTP token using the secret stored in the password store:

	$ plage cat 2fa/codeberg/op | totp
	722524

Interactively edit the contents of
*entry/name*
with
mg(1):

	$ env VISUAL=mg plage edit entry/name

Display the entries matching
'key'
arranged comfortably for reading in a terminal window:

	$ plage find key | rs

Enable tab-completion of
**plage**
command names and entries in
ksh(1):

	$ set -A complete_plage_1 -- cat edit find mv rm tee
	$ set -A complete_plage -- $(plage find)

A simple script to migrate an existing pass(1) password store to plage:

	#!/bin/sh

	cd ~/.password-store || exit 1

	TMPFILE=$(mktemp /tmp/passmig.XXXXXXXXX) || exit 1
	find . -not -path '*/.git*' -not -path '*/.gpg-id' -not -path '.' -iname '*.gpg' > "${TMPFILE}"

	while read -r passfile; do
        name="${passfile#./}"; name="${name%.gpg}"
        pass show "${name}" | plage tee "${name}"
	done < "${TMPFILE}"

Note: This will print every single password it migrates to the console, 
so it's unsuitable for untrusted surroundings.

# SEE ALSO

got(1),
age(1),
pwg(1),
totp(1)

# HISTORY

**plage**
Is based on
plass(1)
and adapted to work with age, including ideas from passage.

# AUTHORS

The
**plage**
utility was written by
Volker Schlecht &lt;[volker@schlecht.dev](mailto:volker@schlecht.dev)&gt;.
The
plass(1)
utility was written by
Omar Polo &lt;[op@omarpolo.com](mailto:op@omarpolo.com)&gt;

# CAVEATS

**plage**
**find**
output format isn't designed to handle files containing newlines.
Use
find(1)
**-print0**
or similar if it's a concern.

**plage**
**mv**
is not able to move directory trees, only file entries.

There isn't an
**init**
sub-command, the store initialization must be performed manually.
