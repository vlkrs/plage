.\" Copyright (c) 2023 Volker Schlecht <volker@schlecht.dev>
.\" Copyright (c) 2022, 2023 Omar Polo <op@omarpolo.com>
.\"
.\" Permission to use, copy, modify, and distribute this software for any
.\" purpose with or without fee is hereby granted, provided that the above
.\" copyright notice and this permission notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
.\" WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
.\" ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
.\" WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
.\" ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.Dd January 23, 2023
.Dt PLAGE 1
.Os
.Sh NAME
.Nm plage
.Nd manage passwords with age
.Sh SYNOPSIS
.Nm
.Op Fl h
.Ar command
.Op Ar argument ...
.Sh DESCRIPTION
.Nm
is a password manager.
Every password lives inside a
.Xr age 1
encrypted file somewhere inside
.Pa ~/.plage/store
which is managed with the
.Xr got 1
version control system to keep track of changes, recover accidental
overwrites and synchronize passwords across devices.
.Pp
The options are as follows:
.Bl -tag -width Ds
.It Fl h
Display usage information and exit immediately.
.El
.Pp
The following commands are available:
.Bl -tag -width Ds
.It Cm cat Ar entries ...
Decrypt and print the content of
.Ar entries
in the given order.
.It Cm edit Ar entry
Interactively modify the content of the given
.Ar entry
with an editor.
.It Cm find Op Ar pattern
Print the entries of the store one per line, optionally filtered by
the case-insensitive
.Ar pattern .
.It Cm mv Ar from Ar to
Rename a password entry, doesn't work with directories.
.Ar from
must exist and
.Ar to
mustn't.
.It Cm rm Ar entries ...
Remove the given
.Ar entries
from the store.
.It Cm tee Oo Fl q Oc Ar entry
Persist the data read from standard input into the store under the given
.Ar entry
name and then print it again on the standard output unless the
.Fl q
option is given.
.It Cm version
Print version and exit
.El
.Pp
Password entries can be referenced using the path relative to the
store directory.
The file extension
.Dq \&.age
can be omitted.
.Sh ENVIRONMENT
.Bl -tag -width Ds
.It Ev PLAGE_AGE
Path to the
.Xr age 1
executable.
.It Ev PLAGE_STORE
Alternative path to the password store directory tree.
.It Ev PLAGE_IDENTITY
Alternative path to the age identity file.
.It Ev PLAGE_RECIPIENTS
Alternative path to the age recipients file.
.It Ev VISUAL , Ev EDITOR
The editor spawned by
.Nm
.Cm edit .
If not set, the
.Xr ed 1
text editor will be used to give it the attention it deserves.
.El
.Sh FILES
.Bl -tag -width Ds
.It Pa ~/.plage
Default password store base directory.
.It Pa ~/.plage/.age-id
age identity file, this should be either an encrypted age file, or a file
with key handles generated from age-plugin-yubikey.
.It Pa ~/.plage/store/
Default password store.
.It Pa ~/.plage/store/.recipients
age recipients file containing the recipient(s) to which the password are
encrypted.
.El
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
A got repository and password store can be initialized as follows:
.Bd -literal -offset indent
$ mkdir -p ~/.plage/store
$ KEY="$(age-keygen)"
$ echo "$KEY" | age -p -a >> ~/.plage/.age-id
$ echo "$KEY" | age-keygen -y >> ~/.plage/store/.recipients
$ gotadmin init ~/got/plage.git
$ got import -r ~/got/plage.git -m 'initial import' ~/.plage/store
$ got checkout -E ~/got/plage.git ~/.plage/store
.Ed
.Pp
see
.Xr got 1
for more information.
.Pp
Generate a random password and save it to the clipboard:
.Bd -literal -offset indent
$ pwg | plage tee entry/name | xsel -b
.Ed
.Pp
Generate a TOTP token using the secret stored in the password store:
.Bd -literal -offset indent
$ plage cat 2fa/codeberg/op | totp
722524
.Ed
.Pp
Interactively edit the contents of
.Pa entry/name
with
.Xr mg 1 :
.Bd -literal -offset indent
$ env VISUAL=mg plage edit entry/name
.Ed
.Pp
Display the entries matching
.Sq key
arranged comfortably for reading in a terminal window:
.Bd -literal -offset indent
$ plage find key | rs
.Ed
.Pp
Enable tab-completion of
.Nm
command names and entries in
.Xr ksh 1 :
.Bd -literal -offset indent
$ set -A complete_plage_1 -- cat edit find mv rm tee
$ set -A complete_plage -- $(plage find)
.Ed
.Sh SEE ALSO
.Xr got 1 ,
.Xr age 1 ,
.Xr pwg 1 ,
.Xr totp 1
.Sh HISTORY
.Nm
Is based on
.Xr plass 1
and adapted to work with age, including ideas from passage.
.Sh AUTHORS
.An -nosplit
The
.Nm
utility was written by
.An Volker Schlecht Aq Mt volker@schlecht.dev .
The
.Xr plass 1
utility was written by
.An Omar Polo Aq Mt op@omarpolo.com
.Sh CAVEATS
.Nm
.Cm find
output format isn't designed to handle files containing newlines.
Use
.Xr find 1
.Fl print0
or similar if it's a concern.
.Pp
.Nm
.Cm mv
is not able to move directory trees, only file entries.
.Pp
There isn't an
.Cm init
sub-command, the store initialization must be performed manually.
